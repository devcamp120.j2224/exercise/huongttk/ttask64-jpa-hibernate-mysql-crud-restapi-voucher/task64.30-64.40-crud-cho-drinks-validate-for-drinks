package com.drinks.drinkapi.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.drinks.drinkapi.model.CDrink;
import com.drinks.drinkapi.repository.CDrinkRepository;

@CrossOrigin
@RestController
@RequestMapping
public class CDrinkController {
    @Autowired
    CDrinkRepository pDrinkRepository;

    // Viết method get list drink: getAllDrinks()
    @GetMapping("/drinks")
    public ResponseEntity<List<CDrink>> getAllDrinks() {
        try {
            List<CDrink> listDrink = new ArrayList<CDrink>();
            pDrinkRepository.findAll().forEach(listDrink::add);
            return new ResponseEntity<>(listDrink, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // Viết method get list drink: getDrinkById()
    @GetMapping("/drinks/{id}")
	public ResponseEntity<CDrink> getDrinkById(@PathVariable("id") long id) {
        try {
                Optional<CDrink> drinkData = pDrinkRepository.findById(id);
            if (drinkData.isPresent()) {
                return new ResponseEntity<>(drinkData.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
     // Viết method create drink: createDrink()
     @PostMapping("/drinks")
     public ResponseEntity<CDrink> createDrink(@Valid @RequestBody CDrink pDrink) {
         try {
            pDrink.setNgayTao(new Date());
            pDrink.setNgayCapNhat(null);
 
            CDrink _drinks = pDrinkRepository.save(pDrink);
             return new ResponseEntity<>(_drinks, HttpStatus.CREATED);
         } catch (Exception e) {
             System.out.println(e);
             return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
         }
     }
     // Viết method update drink: updateDrink()
    @PutMapping("/drinks/{id}")
    public ResponseEntity<CDrink> updateCVoucherById(@PathVariable("id") long id, @Valid @RequestBody CDrink pDrink) {
        try {
            Optional<CDrink> drinkData = pDrinkRepository.findById(id);

        if (drinkData.isPresent()) {
            CDrink drink = drinkData.get();
            drink.setMaNuocUong(pDrink.getMaNuocUong());
            drink.setTenNuocUong(pDrink.getTenNuocUong());
            drink.setDonGia(pDrink.getDonGia());
            drink.setNgayCapNhat(new Date());
            return new ResponseEntity<>(pDrinkRepository.save(drink), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
     // Viết method delete drink: deleteDrink()
     @DeleteMapping("/drinks/{id}")
     public ResponseEntity<CDrink> deleteDrink(@PathVariable("id") long id) {
         try {
            pDrinkRepository.deleteById(id);
             return new ResponseEntity<>(HttpStatus.NO_CONTENT);
         } catch (Exception e) {
             System.out.println(e);
             return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
         }
     }
     
    
}
