package com.drinks.drinkapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.drinks.drinkapi.model.CDrink;

public interface CDrinkRepository extends JpaRepository <CDrink, Long>{
    
}
